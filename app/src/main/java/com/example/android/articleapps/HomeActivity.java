package com.example.android.articleapps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {
    TextView greeting;
    private boolean isNightModeEnabled = false;
    int themeId = 0;

    @Override
    public void setTheme(int resid) {
        super.setTheme(resid);
        themeId = resid;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setupSharedPreferences();
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        greeting = findViewById(R.id.txGreeting);
//        PreferenceManager.setDefaultValues(this, R.xml.setting_pref, false);
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("nightMode",false)){
            greeting.setText("Good Night");
        }else{
            greeting.setText("Good Morning");
        }
        //Design Greeting
        if (prefs.getBoolean("bigSize",false)){
            greeting.setTextSize(getResources().getDimension(R.dimen.big)+12);
        }else{
            greeting.setTextSize(getResources().getDimension(R.dimen.small)+12);
        }

    }

    private void setupSharedPreferences() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        toggleTheme(prefs.getBoolean("nightMode",false));

    }

    public void toggleTheme(Boolean bo){
        if (bo){
            setTheme(R.style.dark);
        }else{
            setTheme(R.style.light);
        }
    }

    public void list(View view){
        startActivity(new Intent(HomeActivity.this,ListArticleActivity.class));
    }

    public void input(View view){
        startActivity(new Intent(HomeActivity.this,InputArticleActivity.class));
    }

    public void setting(View view){
        startActivity(new Intent(HomeActivity.this, SettingActivity.class));
        finish();
    }
}
