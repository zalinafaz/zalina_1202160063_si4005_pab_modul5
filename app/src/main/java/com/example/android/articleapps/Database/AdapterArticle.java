package com.example.android.articleapps.Database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.articleapps.DetailActivity;
import com.example.android.articleapps.R;

import java.util.ArrayList;

public class AdapterArticle extends RecyclerView.Adapter<AdapterArticle.ViewHolder>{
    private ArrayList<Artikel> listArtikel;
    private Context mContext;

    public AdapterArticle(ArrayList<Artikel> listArtikel, Context mContext) {
        this.listArtikel = listArtikel;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AdapterArticle.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.menu_card, viewGroup, false)) {
        };
    }

    @Override
    public void onBindViewHolder(AdapterArticle.ViewHolder holder, int position) {
        Artikel artikel = listArtikel.get(position);
        holder.bindTo(artikel);

    }
    public int getItemCount() {
        return listArtikel.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView judul,author,desc;
        private String date,isi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            SharedPreferences prefs = mContext.getSharedPreferences(mContext.getPackageName(), mContext.MODE_PRIVATE);

            judul = itemView.findViewById(R.id.txJudulCard);
            author = itemView.findViewById(R.id.txAuthor);
            desc = itemView.findViewById(R.id.txDesc);

            if (prefs.getBoolean("bigSize",false)){
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.big)+14);
                author.setTextSize(itemView.getResources().getDimension(R.dimen.big)-10);
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.big));
            }else{
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.small)+14);
                author.setTextSize(itemView.getResources().getDimension(R.dimen.small)-12);
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.small));
            }

            itemView.setOnClickListener(this);
        }

        @SuppressLint("StaticFieldLeak")
        public void bindTo(final Artikel artikel) {
            judul.setText(artikel.getTitle());
            author.setText(artikel.getAuthor());
            if (artikel.getDescription().length()>20){
                desc.setText((artikel.getDescription().substring(0,20)+"..."));
            }else {
                desc.setText(artikel.getDescription());
            }
            date = artikel.getCreated_at();
            isi = artikel.getDescription();
        }

        @Override
        public void onClick(View v) {
//            Toast.makeText(mContext, judul.getText().toString(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra("judul",judul.getText().toString());
            intent.putExtra("isi",isi);
            intent.putExtra("date",date);
            intent.putExtra("author",author.getText().toString());
            mContext.startActivity(intent);
        }

    }
}
